<?php
namespace App\PaymentGateway\PayPal;

use App\Enums\Status;
use http\Exception\InvalidArgumentException;

class Transaction
{
    private string $status;
    public static int $count = 0; //statikus változó

    public function __construct(
        public float $amount,
        public string $description
    )
    {
        self::$count++;
        $this->setStatus(Status::PENDING);
        //var_dump(self::STATUS_PENDING);
    }

    public function process(){
        echo 'Paypal fizetés feldolgozás folyamatban!';
    }

    public static function getCount(): int{
        return self::$count;
    }

    public function setStatus(string $status):self
    {
        if (!array_key_exists($status, Status::ALL_STATUSES)){
            throw new \InvalidArgumentException('Nem érvényes státusz lett megadva!');
        }
        $this->status = $status;

        return $this;
    }

}
