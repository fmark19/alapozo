<?php
//MVC (ModelViewController) != OOP (Object Oriental Programming)

class Product
{
    /*private float $price;
    private string $name;*/

    public function __construct(
        private string $name,
        private float $price)
    {
        //$this->name = $name . ' - teszt';
        //$this->price = $price;
    }


    public function addTax(float $rate)
    {
        $this->price += $this->price * $rate / 100;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function addDiscount(
        float $rate)
    {
        $this->price -= $this->price * $rate /100;
    }

    public function getName()
    {
        return $this->name;
    }

    //termék kiírása
    public function display()
    {
        $output = '<article class="product">';
        $output .= "<h2>{$this->name}</h2>";
        $output .= "<div><b>Ára: </b>{$this->price}.-HUF</div>";
        $output .= '</article>';
        return $output;
    }

    public function __toString(): string
    {
        return $this->display();
    }

    public function __destruct()
    {
        echo 'Fut a destructor';
    }
}
