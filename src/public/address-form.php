<?php
    if (!empty($_POST)){
        echo '<pre>'.var_export($_POST, true).'</pre>';

        $errors = [];

        $address_line_1 = trim(filter_input(INPUT_POST, 'address_line_1', FILTER_SANITIZE_SPECIAL_CHARS));

        if (!$address_line_1){
             $errors['address_line_1'] = '<div class="alert alert-danger">You must fill this field!</div>';
        }

        $address_line_2 = trim(filter_input(INPUT_POST, 'address_line_2', FILTER_SANITIZE_SPECIAL_CHARS));

        $postal_code = trim(filter_input(INPUT_POST, 'postal_code', FILTER_SANITIZE_SPECIAL_CHARS));


        if (!$postal_code){
            $errors['postal_code'] = '<div class="alert alert-danger">You must fill this field!</div>';
        }else{
            if (is_numeric($postal_code)) {
                if ($postal_code < 1000){
                    $errors['postal_code'] = '<div class="alert alert-danger">The value is too small</div>';
                }
                if ($postal_code > 9999){
                    $errors['postal_code'] = '<div class="alert alert-danger">The value is too high</div>';
                }
            }else{
                $errors['postal_code'] = '<div class="alert alert-danger">Invalid character(s)!</div>';
            }
        }




        $city_name = trim(filter_input(INPUT_POST, 'city_name', FILTER_SANITIZE_SPECIAL_CHARS));

        if (!$city_name){
            $errors['city_name'] = '<div class="alert alert-danger">You must fill this field!</div>';
        }


        $country_name = trim(filter_input(INPUT_POST, 'country_name', FILTER_SANITIZE_SPECIAL_CHARS));

        if (!$country_name){
            $errors['country_name'] = '<div class="alert alert-danger">You must fill this field!</div>';
        }




        if (empty($errors)){
            die('Minden ok');
        }
    }

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Address form</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-6 offset-lg-3">
            <h1>Your address</h1>
            <form method="post">
            <div class="mb-3">
                <label for="address_line_1" class="form-label">Address line 1</label>
                <?php
                    echo $errors['address_line_1'] ?? '';
                ?>
                <input type="text" name="address_line_1" value="<?php echo getValue('address_line_1')?>" class="form-control" id="address_line_1" aria-describedby="address_line_1_Help">
                <div id="address_line_1_Help" class="form-text">...mező információ helye</div>
            </div>
            <div class="mb-3">
                <label for="address_line_2" class="form-label">Address line 2</label>
                <?php
                    echo $errors['address_line_2'] ?? '';
                ?>
                <input type="text" name="address_line_2" value="<?php echo getValue('address_line_2')?>" class="form-control" id="address_line_2">
            </div>
            <div class="mb-3">
                <label for="postal_code" class="form-label">Postal code</label>
                <?php
                    echo $errors['postal_code'] ?? '';
                ?>
                <input type="text" name="postal_code" value="<?php echo getValue('postal_code')?>" class="form-control" id="postal_code">
            </div>
            <div class="mb-3">
                <label for="city_name" class="form-label">City</label>
                <?php
                    echo $errors['city_name'] ?? '';
                ?>
                <input type="text" name="city_name" value="<?php echo getValue('city_name')?>" class="form-control" id="city_name">
            </div>
            <div class="mb-3">
                <label for="country_name" class="form-label">Country</label>
                <?php
                    echo $errors['country_name'] ?? '';
                ?>
                <input type="text" name="country_name" value="<?php echo getValue('country_name')?>" class="form-control" id="country_name">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>
</html>

<?php
function getValue(string $fieldName): ?string{
    return filter_input(INPUT_POST, $fieldName);
}