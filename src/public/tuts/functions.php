<?php
/*
$a = rand(1, 10);
$b = rand(1, 10);

//összeadás
function sum ($a, $b){
    return $a + $b;
}

//kivonás
function subt ($a, $b){
    return $a - $b;;
}

//szorzás
function multip ($a, $b){
    return $a * $b;
}

//osztás
function divi ($a, $b){
    return $a / $b;
}

//hatványozás
function power ($a, $b){
    return pow($a, $b);
}

echo "a = ".$a."<br>";
echo "b = ".$b."<br>";
echo "<br>";
echo "Sum = ".sum($a, $b)."<br>";
echo "Subt = ".subt($a, $b)."<br>";
echo "Multip = ".multip($a, $b)."<br>";
echo "Divi = ".divi($a, $b)."<br>";
echo "Power = ".power($a, $b)."<br>";


echo "----------------------------------"."<br>";

*/

declare(strict_types=1);

function name(): mixed{
    return rand(1, 100) % 2 === 0 ? 50 : 'nonono';
}

echo name();
var_dump(name());

//szorzás
function multiply( int|float $a, int|float $b = 10): int|float {
    return $a * $b;
}

echo '<br>';
$result = multiply(3, 5);
var_dump($result);

function multiplyDouble(int|float &$a, int|float $b): int|float {
    $a *= 2;
    return $a * $b;
}

$a = 3;
$b = 4;

echo '<pre>';
echo multiplyDouble($a, $b);
var_dump($a, $b);


function multiplyAnyAmountOfNumbers(int|float ...$numbers): int|float {
    $ret = 1;
    foreach ($numbers as $number){
          $ret *= $number;
    }
    return $ret;
}

$numbers = [1, 2, 3, 4, 5];

echo multiplyAnyAmountOfNumbers(...$numbers);
var_dump($numbers);
var_dump(...$numbers);

//osztás

function divide(int|float $a, int|float $b): int|float|bool {
    if ($b === 0) {
        return false;
    }elseif ($a % $b === 0) {
        return fdiv($a,$b);
    }
    return $a;
}

$a = 8;
$b = 4;

echo divide(b: $b, a: $a);
echo divide($a, b: $b);

$array = [
    'a' => 8,
    'b' => 4
];
echo divide(...$array);

//setcookie('test1', 'value', 0, '', '', false, true);
//setcookie('test2', 'value2', httponly: true);
echo '<br>';

test();
function test(){
    //global $a;
    echo $GLOBALS['a'];
}
var_dump($a);

//változónév eljárások
$a = 9;
$b = 3;

$fv = 'div';
if (is_callable($fv)){
    echo $fv($a, $b);
}
else{
    echo 'Nincs ilyen eljárás: '. $fv;
}