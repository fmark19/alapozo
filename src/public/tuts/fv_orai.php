<?php

function stringChange($testString){
    //Megadott szöveg kiírás
    echo 'Megadott szöveg: '.'<br>';
    echo $testString.'<br>';
    echo '<br>';

    $arrReplCharsFrom= ['á','é','í','ö','ó','ő','ü','ú','ű','Á','É','Í','Ö','Ó','Ő','Ü','Ú','Ű',' ','?','[',']','.','%','.'];
    $arrReplCharsTo =  ['a','e','i','o','o','o','u','u','u','a','e','i','o','o','o','u','u','u','-','-','-','-','-','-','.'];

    //Kisbetűssé tétel és karakter csere.
    $testString = strtolower(str_replace($arrReplCharsFrom,$arrReplCharsTo,$testString));

    //Végeredmény és trimmelés
    echo 'Végeredmény: '.'<br>';
    echo trim($testString, '\_\-');

}

//Bemenet
stringChange('2021-ÁrvÍzTŰrő% tükÖRFúrógÉP---_[].??.txt??');
echo '<br>';
echo '<br>';
echo '<br>';
echo '<br>';
stringChange('2021-ÁrvÍzTŰrő% tükÖRFúrógÉP---_[].??.txt');