<?php
//osztály betöltése
require_once '../../Product.php';
echo '<pre>';
//objektum eólétrehozása

$product = new Product('Termék 1', 1600);
var_dump($product);
//die();


/*$product->name = 'Termék 1 új';
$product->price = 2000.00;
var_dump($product);*/

echo 'Termék ára: '.$product->getPrice();

$product2 = new Product('Termék 2', 1990.00);
var_dump($product2);

$product2->addTax(27);
$product2->addDiscount(50);
var_dump($product2);

echo $product->display();
echo $product2->display();

echo $product;
unset($product);
$product2 = null;


$product3 = new Product('Termék 3', 10000);
//26%Áfa, 10% kedvezmény
var_dump($product3);
echo $product3->addTax(27)
    ->addDiscount(10)
    ->display();

$product4 = (new Product('Termék 4', 25000))
    ->addTax(27)
    ->addDiscount(10);
echo $product4;
