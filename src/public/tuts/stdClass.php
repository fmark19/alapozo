<?php
$test = '{"a":1,"b":2,"c":3}';
$myArray = json_decode($test, true);
echo '<pre>';
var_dump($myArray);
$myObject = json_decode($test);
var_dump($myObject->b);

$object = new stdClass();
$object->valami = 'Hello';
var_dump($object);

$myArray = [1, 2, 3];
$object = (object) $myArray;
var_dump($object->{1});

$data = 5;
$object = (object) $data;
var_dump($object);
echo $object->scalar;
