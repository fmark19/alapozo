<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Cím megadása</title>
</head>
<body>
<div class="container-fluid">
    <form method="post">
        <div class="form-group mx-3 my-3 col-sm-8 col-md-7 col-lg-6 col-xl-4">
            <label for="address1">Cím 1:</label>
            <input class="form-control" type="text" name="address1" id="address1" >
        </div>
        <div class="form-group mx-3 my-3 col-sm-8 col-md-7 col-lg-6 col-xl-4">
            <label for="address2">Cím 2:</label>
            <input class="form-control"  type="text" name="address2" id="address2">
        </div>
        <div class="form-group mx-3 my-3 col-sm-8 col-md-7 col-lg-6 col-xl-4">
            <label for="postcode">Irányítószám:</label>
            <input class="form-control" type="text" name="postcode" id="postcode" maxlength="4" >
        </div>
        <div class="form-group mx-3 my-3 col-sm-8 col-md-7 col-lg-6 col-xl-4">
            <label for="settlement">Település:</label>
            <input class="form-control"  type="text" name="settlement" id="settlement" >
        </div>
        <div class="form-group mx-3 my-3 col-sm-8 col-md-7 col-lg-6 col-xl-4">
            <label for="country">Ország:</label>
            <input class="form-control"  type="text" name="country" id="country" >
        </div>
        <div class="form-group mx-3 my-3">
            <button type="submit" class="btn btn-primary py-2 px-4">Küldés</button>
        </div>
    </form>

    <?php
    $address1 = filter_input(INPUT_POST, "address1", FILTER_SANITIZE_SPECIAL_CHARS);
    $address2 = filter_input(INPUT_POST, "address2", FILTER_SANITIZE_SPECIAL_CHARS);
    $postcode = filter_input(INPUT_POST, "postcode", FILTER_SANITIZE_SPECIAL_CHARS);
    $settlement = filter_input(INPUT_POST, "settlement", FILTER_SANITIZE_SPECIAL_CHARS);
    $country = filter_input(INPUT_POST, "country", FILTER_SANITIZE_SPECIAL_CHARS);
    $message = "";

    //Cím 1:
    if (empty($address1)){
        $message .= "HIBA: A mezőt KÖTELEZŐ kitölteni! (Cím 1:)"."<br>";
    }else{
        $message .= "ÜZENET: Sikeresen megadva! (Cím 1: $address1)"."<br>";
    }

    //Cím 2:
    if (empty($address2)){
        $message .= "ÜZENET: Az adott mezőt NEM KÖTELEZŐ kitölteni (Cím 2:)"."<br>";
    }
    else{
        $message .= "ÜZENET: Sikeresen megadva! (Cím 2: $address2)"."<br>";
    }

    //Irányítószám
    if (empty($postcode)){
        $message .= "HIBA: A mezőt KÖTELEZŐ kitölteni! (Irányítószám:)"."<br>";
    }else{
        if (is_numeric($postcode)){
            $message .= "ÜZENET: Sikeres megadva! (Irányítószám: $postcode)"."<br>";
        }else{
            $message .= "HIBA: Csak számot adhat meg!"."<br>";
        }
    }

    //Település
    if (empty($settlement)){
        $message .= "HIBA: A mezőt KÖTELEZŐ kitölteni! (Település:)"."<br>";
    }else{
        $message .= "ÜZENET: Sikeres megadva! (Település: $settlement)"."<br>";
    }

    if (empty($country)){
        $message .= "HIBA: A mezőt KÖTELEZŐ kitölteni! (Ország: )"."<br>";
    }else {
        $message .= "ÜZENET: Sikeres megadva! (Ország: $country)" . "<br>";
    }
    ?>

    <ul class="list-group">
        <li class="list-group-item mx-3 col-sm-8 col-md-7 col-lg-6 col-xl-4"><?php echo "Cím 1: ".$address1 ?></li>
        <li class="list-group-item mx-3 col-sm-8 col-md-7 col-lg-6 col-xl-4"><?php echo "Cím 2: ".$address2 ?></li>
        <li class="list-group-item mx-3 col-sm-8 col-md-7 col-lg-6 col-xl-4"><?php echo "Irányítószám: ".$postcode ?></li>
        <li class="list-group-item mx-3 col-sm-8 col-md-7 col-lg-6 col-xl-4"><?php echo "Település: ".$settlement ?></li>
        <li class="list-group-item mx-3 col-sm-8 col-md-7 col-lg-6 col-xl-4"><?php echo "Ország: ".$country ?></li>
    </ul>

    <div class="mx-3">
        <?php
            echo "<br>";
            echo $message."<br>";
        ?>
    </div>
</div>

<!-- type="number" onkeypress="if(this.value.length===4) return false; -->




<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>