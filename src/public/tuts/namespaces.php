<?php

/*require_once "../../App/PaymentGateway/PayPal/Transaction.php";
require_once "../../App/PaymentGateway/Otp/Transaction.php";
require_once "../../App/PaymentGateway/Otp/CustomerProfile.php";
//require_once "../../App/PaymentGateway/Otp/DateTime.php";
require_once "../../App/Notification/Email.php";*/

spl_autoload_register(function ($classname){
    $classPath = __DIR__.'/../../'.str_replace('\\', '/', $classname).'.php';
    //var_dump($classname);
    if (is_file($classPath)){
        require $classPath;
    }
});

use App\PaymentGateway\Otp;
//use PaymentGateway\Otp\{Transaction as OtpTransaction, CustomerProfile};
use App\PaymentGateway\PayPal\Transaction as PayPalTransaction;

$paypalTransaction = new PayPalTransaction();
var_dump($paypalTransaction);

$otpTransaction = new Otp\Transaction();
var_dump($otpTransaction);

$otpCustomerProfile = new Otp\CustomerProfile();
var_dump($otpCustomerProfile);

/*$transaction = new Transaction();
var_dump($transaction);*/
